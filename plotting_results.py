# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 12:42:07 2016

@author: pieter
"""

import numpy as np
import matplotlib.pyplot as plt

folder = "/home/pieter/Documents/kris_thing/readings/" 
file = "Results.txt"

headers=np.genfromtxt(folder+file,max_rows=1,dtype='str')
readings = np.genfromtxt(folder+file,skip_header=1)
x=readings[:,0]
y=(readings[:,1]/np.pi)**(0.5)

x=np.insert(x, 0, 0)
y=np.insert(y, 0, 0)

plt.scatter(x,y)
plt.xlabel("Time (arb)")
plt.ylabel("Radius (Pixels)")